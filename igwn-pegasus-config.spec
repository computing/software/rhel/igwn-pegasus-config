Name:      igwn-pegasus-config
Version:   8
Release:   1%{?dist}
Summary:   DNF configuration for IGWN Pegasus Repository.

License:   GPLv3+
BuildArch: noarch
Requires:  redhat-release >= %{version}
URL:       https://software.igwn.org/lscsoft/pegasus/%{version}

Source0:   igwn-pegasus.repo
Source1:   COPYING
Source2:   README-igwn-pegasus.md

%description
DNF configuration for IGWN Pegasus Repository on Rocky Linux %{version}

%prep
%setup -q -c -T
install -pm 644 %{SOURCE1} .
install -pm 644 %{SOURCE2} .

%build

%install
install -dm 755 %{buildroot}%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc README-igwn-pegasus.md
%license COPYING
%config(noreplace) %{_sysconfdir}/yum.repos.d/igwn-pegasus.repo

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Fri Nov 17 2023 Adam Mercer <adam.mercer@ligo.org> 8-1
- initial version
