PACKAGE=$(shell echo *.spec | sed -e 's/.spec$$//')

sources:

srpm:
	rpmbuild -D '_srcrpmdir ./' -D '_sourcedir ./' -D 'dist .el8' -bs $(PACKAGE).spec
