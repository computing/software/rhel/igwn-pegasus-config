# IGWN Pegasus Yum/DNF Repository

The IGWN Pegasus Yum/DNF Repository configuration for the IGWN Pegasus repository.

See <https://computing.docs.ligo.org/guide/software/> for details on
IGWN Software practices.
